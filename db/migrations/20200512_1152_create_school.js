exports.up = knex => knex.schema
    .createTable('careers', table => {
        table.string('id').primary();
        table.string('description');
        table.string('iso2', 2);
        table.boolean('deleted');
        table.timestamp('createdAt');
        table.timestamp('updatedAt');
        table.timestamp('deletedAt');
        table.integer('__v');
    })

    .createTable('years', table => {
        table.string('id').primary();
        table.string('description');
        table.string('iso2', 2);
        table.boolean('deleted');
        table.timestamp('createdAt');
        table.timestamp('updatedAt');
        table.timestamp('deletedAt');
        table.integer('__v');
    })

    .createTable('subjects', table => {
        table.string('id').primary();
        table.string('description');
        table.string('iso2', 2);
        table.boolean('deleted');
        table.timestamp('createdAt');
        table.timestamp('updatedAt');
        table.timestamp('deletedAt');
        table.integer('__v');
    })

    .createTable('teachers', table => {
        table.string('id').primary();
        table.string('name');
        table.string('surname');
        table.string('phone');
        table.string('home');
        table.string('email');
        table.string('iso2', 2);
        table.boolean('deleted');
        table.timestamp('createdAt');
        table.timestamp('updatedAt');
        table.timestamp('deletedAt');
        table.integer('__v');
    })

    .createTable('students', table => {
        table.string('id').primary();
        table.string('name');
        table.string('surname');
        table.string('phone');
        table.string('home');
        table.string('email');
        table.string('iso2', 2);
        table.boolean('deleted');
        table.timestamp('createdAt');
        table.timestamp('updatedAt');
        table.timestamp('deletedAt');
        table.integer('__v');
    })

    .createTable('assignedSubjects', table => {
        table.string('id').primary();
        table.string('careers').references('careers.id');
        table.string('years').references('years.id');
        table.string('subjects').references('subjects.id');
        table.string('teachers').references('teachers.id');
        table.string('students').references('students.id');
        table.string('iso2', 2);
        table.boolean('deleted');
        table.timestamp('createdAt');
        table.timestamp('updatedAt');
        table.timestamp('deletedAt');
        table.integer('__v');
    })

    .createTable('studentNotes', table => {
        table.string('id').primary();
        table.string('note1');
        table.string('note2');
        table.string('note3');
        table.string('averages');
        table.string('subjects').references('subjects.id');
        table.string('students').references('students.id');
        table.string('teachers').references('teachers.id');
        table.string('iso2', 2);
        table.boolean('deleted');
        table.timestamp('createdAt');
        table.timestamp('updatedAt');
        table.timestamp('deletedAt');
        table.integer('__v');
    });

exports.down = knex => knex.schema
    .dropTable('careers')
    .dropTable('years')
    .dropTable('subjects')
    .dropTable('teachers')
    .dropTable('students')
    .dropTable('assignedSubjects')
    .dropTable('studentNotes');
