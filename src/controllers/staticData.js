const fields = require ('../services/field');

class StaticDatasController {
    static fetch (req, res, next){
        try{
            res.send ({fields});
        }catch(error){
            next(error);
        }
    }
}

module.exports = StaticDatasController;
