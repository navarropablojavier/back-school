const { Years } = include('models');

class YearsController {
    static async fetch(req, res, next) {
        try {
            const years = await Years.find({
                ...req.query,
                deleted: null
            });
            res.send(years);
        } catch (error) {
            next(error);
        }
    }

    static async fetchOne(req, res, next) {
        try {
            const year = await Years.findOne(req.params);
            res.send(year);
        } catch (error) {
            next(error);
        }
    }

    static async create(req, res, next) {
        try {
            const result = await Years.insertOne(req.body);
            res.send({
                success: true,
                result
            });
        } catch (error) {
            next(error);
        }
    }

    static async save(req, res, next) {
        try {
            const result = await Years.updateOne(req.params, req.body);
            res.send({
                success: true,
                result
            });
        } catch (error) {
            next(error);
        }
    }

    static async delete(req, res, next) {
        try {
            const result = await Years.deletedOne(req.params.id);
            res.send({
                success: true,
                result
            });
        } catch (error) {
            next(error);
        }
    }
}

module.exports = YearsController;
