const {AssignedSubjects} = include('models');

class AssignedSubjectsController {
    static async fetch (req, res, next) {
        try{
            const assignedSubjects = await AssignedSubjects.find({
                ...req.query,
                deleted: null
            });
            res.send(assignedSubjects);
        }catch(error){
            next(error);
        }
    }
    static async create(req, res, next) {
        try{
            const result = await AssignedSubjects.insertOne(req.body);
            res.send({
                status: 'success',
                result
            });
        }catch(error){
            next(error);
        }
    }
    static async fetchOne(req, res, next) {
        try{
            const assignedSubject = await AssignedSubjects.findOne({id: req.params.id});
            res.send(assignedSubject);
        }catch(error){
            next(error);
        }
    }
    static async save(req, res, next) {
        try{
            const result = await AssignedSubjects.updateOne({id: req.params.id}, req.body);
            res.send(result);
        }catch(error){
            next(error);
        }
    }
    static async delete (req, res, next){
        try{
            const result = await AssignedSubjects.deletedOne(req.params.id);
            res.send({
                succes: true,
                result
            });
        }catch(error){
            next(error);
        }
    }
}

module.exports = AssignedSubjectsController;
