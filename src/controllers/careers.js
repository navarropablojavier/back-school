const {Careers} = include('models');

class CareersController {
    static async fetch (req, res, next){
        try{
            const careers = await Careers.find({
                ...req.query,
                deleted: null
            });
            res.send(careers);
        }catch (error){
            next(error);
        }
    }
    static async create (req, res, next ){
        try{
            const result = await Careers.insertOne(req.body);
            res.send({
                status: 'success',
                result
            });
        }catch(error){
            next(error);
        }
    }
    static async fetchOne (req, res, next) {
        try{
            const career = await Careers.findOne({id: req.params.id});
            res.send(career);
        }catch(error){
            next(error);
        }
    }
    static async save (req, res, next) {
        try{
            const result = await Careers.updateOne({id: req.params.id},
                req.body
            );
            res.send(result);
        }catch(error){
            next(error);
        }
    }
    static async delete (req, res, next){
        try{
            const result = await Careers.deletedOne(req.params.id);
            res.send({
                succes: true,
                result
            });
        }catch(error) {
            next(error);
        }
    }
}

module.exports = CareersController;
