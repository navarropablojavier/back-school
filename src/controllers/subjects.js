const {Subjects} = include('models');

class SubjectsController {
    static async fetch (req, res, next) {
        try{
            const subjects = await Subjects.find({
                ...req.query,
                deleted: null
            });
            res.send(subjects);
        }catch(error){
            next(error);
        }
    }
    static async create(req, res, next) {
        try{
            const result = await Subjects.insertOne(req.body);
            res.send({
                status: 'success',
                result
            });
        } catch(error) {
            next(error);
        }
    }
    static async fetchOne(req, res, next){
        try {
            const subject = await Subjects.findOne({id: req.params.id});
            res.send(subject);
        }catch (error){
            next(error);
        }
    }
    static async save (req, res, next) {
        try {
            const result = await Subjects.updateOne({id: req.params.id}, req.body);
            res.send(result);
        }catch(error){
            next(error);
        }
    }
    static async delete(req, res, next) {
        try{
            const result = await Subjects.deletedOne(req.params.id);
            res.send({
                succes: true,
                result
            });
        }catch(error){
            next(error);
        }
    }
}

module.exports = SubjectsController;
