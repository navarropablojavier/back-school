const {Teachers} = include('models');

class TeachersController {
    static async fetch(req, res, next){
        try {
            const teachers = await Teachers.find({
                ...req.query,
                deleted: null
            });
            res.send(teachers);
        }catch (error){
            next(error);
        }
    }

    static async create (req, res, next){
        try{
            const result = await Teachers.insertOne(req.body);
            res.send({
                status: 'success',
                result
            });
        }catch(error){
            next(error);
        }
    }
    static async fetchOne (req, res, next) {
        try{
            const teacher = await Teachers.findOne({id: req.params.id});
            res.send(teacher);
        }catch (error){
            next(error);
        }
    }
    static async save (req, res, next) {
        try{
            const result = await Teachers.updateOne({id: req.params.id},
                req.body
            );
            res.send(result);
        }catch(error){
            next(error);
        }
    }
    static async delete (req, res, next) {
        try{
            const result = await Teachers.deletedOne(req.params.id);
            res.send({
                succes: true,
                result
            });
        }catch (error){
            next(error);
        }

    }

}
module.exports = TeachersController;
