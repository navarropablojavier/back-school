const {StudentNotes} = include('models');

class StudentNotesController {
    static async fetch (req, res, next){
        try{
            const studentNotes = await StudentNotes.find({
                ...req.query,
                deleted: null
            });
            res.send(studentNotes);
        }catch(error){
            next (error);
        }
    }

    static async create (req, res, next) {
        try{
            const result = await StudentNotes.insertOne(req.body);
            res.send({
                status: 'success',
                result
            });
        }catch(error){
            next(error);
        }
    }

    static async fetchOne (req, res, next) {
        try {
            const studentNote = await StudentNotes.findOne ({id: req.params.id});
            res.send(studentNote);
        }catch(error){
            next(error);
        }
    }

    static async save (req, res, next){
        try{
            const result = await StudentNotes.updateOne({id: req.params.id},
                req.body
            );
            res.send(result);
        }catch(error){
            next(error);
        }
    }

    static async delete (req, res, next){
        try {
            const result = await StudentNotes.deletedOne(req.params.id);
            res.send ({
                succes: true,
                result
            });
        }catch (error){
            next (error);
        }
    }
}

module.exports = StudentNotesController;
