const {Students} = include ('models');

class StudentsController {
    static async fetch (req, res, next){
        try{
            const students = await Students.find({
                ...req.query,
                deleted: null
            });
            res.send(students);
        }catch (error){
            next (error);
        }
    }

    static async create (req, res, next){
        try{
            const result = await Students.insertOne(req.body);
            res.send({
                status: 'success',
                result
            });
        }catch(error){
            next(error);
        }
    }

    static async fetchOne (req, res, next) {
        try{
            const student = await Students.findOne({id: req.params.id});
            res.send(student);
        }catch(error){
            next(error);
        }
    }

    static async save (req, res, next) {
        try{
            const result = await Students.updateOne({id: req.params.id},
                req.body
            );
            res.send(result);
        }catch(error){
            next(error);
        }
    }

    static async delete (req, res, next) {
        try {
            const result = await Students.deletedOne(req.params.id);
            res.send({
                succes: true,
                result
            });
        }catch (error){
            next(error);
        }
    }
}

module.exports = StudentsController;
