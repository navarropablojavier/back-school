module.exports = {
    CareersController: require('./careers'),
    YearsController: require('./years'),
    TeachersController: require('./teachers'),
    SubjectsController: require('./subjects'),
    StudentsController: require('./students'),
    AssignedSubjectsController: require('./assignedSubjects'),
    StudentNotesController: require('./studentNotes'),
    StaticDatasController: require ('./staticData'),
    StatusController: require('./status')
};
