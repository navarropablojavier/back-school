const ModelCreate = include ('helpers/modelCreate');
const tableName = 'studentNotes';
const name = 'StudentNotes';
const selectableProps = ['id', 'note1', 'note2', 'note3', 'averages', 'subjects', 'students', 'teachers', 'iso2', 'deleted' ];

class StudentNotes extends ModelCreate {
    constructor (props){
        super({
            ...props,
            tableName,
            name,
            selectableProps
        });
    }
}

module.exports = knex => new StudentNotes ({knex});
