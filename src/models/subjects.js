const ModelCreate = include('helpers/modelCreate');
const tableName = 'subjects';
const name = 'Subjects';
const selectableProps = [
    'id',
    'description',
    'iso2',
    'deleted'
];
class Subjects extends ModelCreate {
    constructor(props){
        super({
            ...props,
            tableName,
            name,
            selectableProps
        });
    }
}

module.exports = knex => new Subjects ({knex});
