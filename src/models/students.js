const ModelCreate = include ('helpers/modelCreate');
const tableName = 'students';
const name = 'Students';
const selectableProps = ['id', 'name', 'surname', 'phone', 'home', 'email', 'iso2', 'deleted' ];

class Students extends ModelCreate {
    constructor (props) {
        super({
            ...(props),
            tableName,
            name,
            selectableProps
        });
    }
}

module.exports =knex => new Students({knex});
