const ModelCreate = include('helpers/modelCreate');
const tableName = 'assignedSubjects';
const name = 'AssignedSubjects';
const selectableProps= [
    'id',
    'careers',
    'years',
    'subjects',
    'teachers',
    'students',
    'iso2',
    'deleted'
];

class AssignedSubjects extends ModelCreate{
    constructor(props){
        super({
            ...props,
            tableName,
            name,
            selectableProps
        });
    }
}

module.exports = knex => new AssignedSubjects({knex});
