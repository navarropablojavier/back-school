const ModelCreate = include('helpers/modelCreate');

const tableName = 'years';
const name = 'Years';
const selectableProps = ['id', 'description', 'iso2', 'deleted'];

class Years extends ModelCreate {
    constructor(props) {
        super({
            ...props,
            tableName,
            name,
            selectableProps
        });
    }
}
module.exports = knex => new Years({ knex });
