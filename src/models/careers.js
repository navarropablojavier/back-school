const ModelCreate = include('helpers/modelCreate');
const tableName = 'careers';
const name = 'Careers';
const selectableProps = ['id', 'description', 'iso2', 'deleted'];

class Careers extends ModelCreate {
    constructor(props) {
        super({
            ...props,
            tableName,
            name,
            selectableProps
        });
    }
}

module.exports = knex => new Careers({ knex });
