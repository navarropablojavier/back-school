const ModelCreate = include('helpers/modelCreate');
const tableName = 'teachers';
const name = 'Teachers';
const selectableProps = ['id', 'name', 'surname', 'phone', 'home', 'email', 'iso2', 'deleted'];

class Teachers extends ModelCreate{
    constructor(props){
        super({
            ...props,
            tableName,
            name,
            selectableProps
        });
    }
}

module.exports = knex => new Teachers ({knex});
