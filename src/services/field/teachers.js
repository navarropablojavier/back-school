module.exports = {
    teachers: [],
    currentTeachers: {
        name: '',
        surmane: '',
        phone: '',
        home:'',
        email: ''
    },
    headers: [ {
        label: 'Nombre',
        key: 'name'
    },
    {
        label: 'Apellido',
        key: 'surname'
    },
    {
        label: 'Telefono',
        key: 'phone'
    },
    {
        label: 'Dirección',
        key: 'home'
    },
    {
        label: 'E-mail',
        key: 'email'
    }
    ]
};
