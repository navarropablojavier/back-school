module.exports = {
    studentNotes: [],
    currentStudentNotes: {
        note1: '',
        note2: '',
        note3: '',
        averages: '',
        subjects: '',
        students: '',
        teachers: ''
    },
    headers: [ {
        label: 'Nota 1',
        key: 'note1'
    },
    {
        label: 'Nota 2',
        key: 'note2'
    },
    {
        label: 'Nota 3',
        key: 'note3'
    },
    {
        label: 'Promedio',
        key: 'averages'
    },
    {
        label: 'Materia',
        key: 'subjects'
    },
    {
        label: 'Estudiante',
        key: 'students'
    },
    {
        label: 'Profesor',
        key: 'teachers'
    }
    ]
};
