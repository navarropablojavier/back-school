const assignedSubjects = require ('./assignedSubjects');
const careers = require ('./careers');
const studentNotes = require ('./studentNotes');
const student = require ('./student');
const subjects = require ('./subjects');
const teachers = require ('./teachers');
module.exports = {
    assignedSubjects,
    careers,
    studentNotes,
    student,
    subjects,
    teachers
};
