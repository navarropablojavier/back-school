module.exports = {
    assignedSubjects: [],
    currentAssignedSubjects: {
        careers: '',
        years: '',
        subjects: '',
        teachers: '',
        students: ''
    },
    headers: [{
        label: 'Carreras ',
        key: 'careers'
    },
    {
        label: 'Años ',
        key: 'years'
    },
    {
        label: 'Materias ',
        key: 'subjects'
    },
    {
        label: 'Profesores ',
        key: 'teachers'
    },
    {
        label: 'Alumnos ',
        key: 'students'
    }]
};
