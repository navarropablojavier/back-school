module.exports = {
    students: [],
    currentStudents: {
        name: '',
        surname: '',
        phone: '',
        home: '',
        email: ''
    },
    headers: [ {
        label: 'Nombre',
        key: 'name'
    },
    {
        label: 'Apellido',
        key: 'surname'
    },
    {
        label: 'Telefono',
        key: 'phone'
    },
    {
        label: 'Direccion',
        key: 'home'
    },
    {
        label: 'E-mail',
        key: 'email'
    }
    ]
};
