module.exports = {

    type: 'object',
    properties:{
        id: {
            type: 'string',
            format: 'uuid',
            nullable: true
        },
        name: {type: 'string'},
        surname: {type: 'string'},
        phone: {type: 'string'},
        home: {type: 'string'},
        email: {type: 'string'}

    }

};
