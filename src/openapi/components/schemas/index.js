const ArrayString = require('./ArrayString');
const ArrayNumber = require('./ArrayNumber');
const Role = require('./Role');
const ids = require('./ids');
const Careers = require('./Careers');
const Years = require ('./Years');
const Subjects = require('./Subjects');
const Teachers = require ('./Teachers');
const Students = require('./Students');
const AssignedSubjects = require ('./AssignedSubjects');
const StudentNotes = require('./StudentNotes');
const Error = require ('./Error');
module.exports = {
    ArrayString,
    ArrayNumber,
    Role,
    ids,
    Careers,
    Years,
    Subjects,
    Teachers,
    Students,
    AssignedSubjects,
    StudentNotes,
    Error
};
