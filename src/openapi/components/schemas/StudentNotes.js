module.exports = {
    type: 'object',
    properties: {
        id:{
            type: 'string',
            format: 'uuid',
            nullable: true
        },
        note1: {type: 'string'},
        note2: {type: 'string'},
        note3: {type: 'string'},
        averages: {type: 'string'},
        subjects: {type: 'string'},
        students:{type: 'string'},
        teachers: {type: 'string'}
    }
};
