module.exports = {

    type: 'array',
    uniqueItems: true,
    items: {
        type: 'string',
        format: 'uuid'
    }

};
