module.exports = {
    type: 'array',
    uniqueItems: true,
    items: { type: 'integer' }

};
