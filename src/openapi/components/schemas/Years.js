module.exports = {

    type: 'object',
    properties: {
        id: {
            type: 'string',
            format: 'uuid',
            nullable: true
        },
        description: { type: 'string' }
    }

};
