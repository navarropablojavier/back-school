module.exports = {
    type: 'object',
    properties: {
        id: {
            type: 'string',
            format: 'uuid',
            nullable: true
        },
        careers: {type: 'string'},
        years: {type: 'string'},
        subjects: {type: 'string'},
        teachers: {type: 'string'},
        students: {type: 'string'}
    }
};
