module.exports = {
    ID:{
        in: 'path',
        name: 'id',
        required: true,
        schema: {
            type: 'string',
            format: 'uuid'
        },
        description: 'ID solicitado'
    }
};
