const schemas = require ('./schemas');
const parameters = require ('./parameters');
const securitySchemes = require ('./security');

module.exports = {
    schemas,
    parameters,
    securitySchemes
};
