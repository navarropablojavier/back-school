module.exports = {
    '/api/teachers': {
        get:{
            security:[],
            summary: 'List Taechers',
            parameters: [],
            responses: {
                200: {
                    description: 'list of teachers',
                    content: {
                        'application/json': {
                            schema: {
                                type: 'array',
                                items: { $ref: '#/components/schemas/Teachers' }
                            }
                        }
                    }
                },
                default: {
                    description: 'Error',
                    content: {'application/json' : {schema: { $ref: '#/components/schemas/Error'}}}
                }
            }
        },
        post: {
            security:[],
            requestBody: {
                description: 'Optional description in *Markdown*',
                required: true,
                content: {'application/json': {schema: {$ref: '#/components/schemas/Teachers'}}}
            },
            responses: {
                200 : {
                    description: 'list of teachers',
                    content: {
                        'application/json': {
                            schema: {
                                type: 'object',
                                properties: {}
                            }
                        }
                    }
                },
                default: {
                    description: 'Error',
                    content: {'application/json': {schema: {$ref: '#/components/schemas/Error'}}}
                }
            }
        }
    },
    '/api/teachers/{id}' : {
        get: {
            security: [],
            summary: 'list of teachers',
            parameters: [{$ref: '#/components/parameters/ID'} ],
            responses: {
                200: {
                    description: 'list of Teachers',
                    content: {'application/json': {schema: {$ref: '#/components/schemas/Teachers'}}}
                },
                default: {
                    description: 'Error',
                    content: {'application/json': {schema: {$ref: '#/components/schemas/Error'}}}
                }
            }
        },
        put: {
            security: [],
            parameters: [{$ref: '#/components/parameters/ID'} ],
            requestBody: {
                description: 'Optional description in *Markdown*',
                required: true,
                content: {'application/json': {schema: {$ref: '#/components/schemas/Teachers'}}}
            },
            responses:{
                200: {
                    description: 'List of teachers',
                    content: {
                        'application/json': {
                            schema: {
                                type: 'object',
                                properties: {}
                            }
                        }
                    }
                },
                default: {
                    description: 'Error',
                    content: {'application/json': {schema: {$ref: '#/components/schemas/Error'}}}
                }
            }
        },
        delete: {
            security: [],
            parameters: [{$ref: '#/components/parameters/ID'} ],
            responses: {
                200: {
                    description: 'List of teachers',
                    content: {
                        'application/json': {
                            schema: {
                                type: 'object',
                                properties: {}
                            }
                        }
                    }
                },
                default: {
                    description: 'Error',
                    content: {'application/json': {schema: {$ref:'#/components/schemas/Error'}}}
                }
            }
        }
    }
};
