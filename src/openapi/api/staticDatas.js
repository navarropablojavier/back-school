module.exports = {
    '/api/staticDatas': {
        get: {
            security: [],
            summary: 'Assigned data to do list',
            parameters: [],
            responses: {
                200: {
                    description: 'Assigned data to do list',
                    content: {
                        'application/json': {
                            schema: {
                                type: 'object',
                                properties:{}
                            }
                        }
                    }
                },
                default: {
                    description: 'Error',
                    content: {'application/json': {schema: {$ref: '#/components/schemas/Error'}}}
                }
            }
        }
    }
};
