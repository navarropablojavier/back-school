module.exports = {
    '/api/assignedSubjects': {
        get: {
            security: [],
            summary: 'Assigned subjects to do list',
            parameters: [],
            responses: {
                200: {
                    description: 'Assigned subjects to do list',
                    content: {
                        'application/json': {
                            schema: {
                                type: 'array',
                                items: {$ref: '#/components/schemas/AssignedSubjects'}
                            }
                        }
                    }
                },
                default: {
                    description: 'Error',
                    content: {'application/json': {schema: {$ref: '#/components/schemas/Error'}}}
                }
            }
        },
        post:{
            security: [],
            requestBody: {
                description: 'Optional description in *Markdown*',
                required: true,
                content: {'application/json': {schema: {$ref:'#/components/schemas/AssignedSubjects'}}}
            },
            responses: {
                200 : {
                    description: 'Assigned subjects to do list',
                    content: {
                        'application/json': {
                            schema: {
                                type: 'object',
                                properties: {}
                            }
                        }
                    }
                },
                default: {
                    description: 'Error',
                    content: {'application/json': {schema: {$ref: '#/components/schemas/Error'}}}
                }
            }
        }
    },
    '/api/assignedSubjects/{id}': {
        get: {
            security: [],
            summary: 'Assigned subjects to do list',
            parameters: [{$ref: '#/components/parameters/ID'} ],
            responses: {
                200: {
                    description: 'Assigned subjects to do list',
                    content: {'application/json':{schema: {$ref: '#/components/schemas/AssignedSubjects'}}}
                },
                default: {
                    description: 'Error',
                    content: {'application/json': {schema: {$ref: '#/components/schemas/Error'}}}
                }
            }
        },
        put: {
            security: [],
            parameters: [{$ref: '#/components/parameters/ID'} ],
            requestBody: {
                description: 'Optional description in *Markdown*',
                required: true,
                content: {'application/json': {schema: {$ref: '#/components/schemas/AssignedSubjects'}}}
            },
            responses:{
                200: {
                    description: 'Assigned subjects to do list',
                    content: {
                        'application/json': {
                            schema: {
                                type: 'object',
                                properties: {}
                            }
                        }
                    }
                },
                default: {
                    description: 'Error',
                    content: {'application/json': {schema: {$ref: '#/components/schemas/Error'}}}
                }
            }
        },
        delete: {
            security: [],
            parameters: [{$ref: '#/components/parameters/ID'} ],
            responses: {
                200: {
                    description: 'Assigned subjects to do list',
                    content: {
                        'application/json': {
                            schema: {
                                type: 'object',
                                properties: {}
                            }
                        }
                    }
                },
                default: {
                    description: 'Error',
                    content: {'application/json': {schema: {$ref:'#/components/schemas/Error'}}}
                }
            }
        }
    }
};
