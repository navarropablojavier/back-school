const careers = require ('./careers');
const years = require('./years');
const subjects = require('./subjects');
const teachers = require('./teachers');
const students = require('./students');
const assignedSubjects = require('./assignedSubjects');
const studentNotes = require('./studentNotes');
const staticDatas = require('./staticDatas');

module.exports = {
    ...careers,
    ...years,
    ...subjects,
    ...teachers,
    ...students,
    ...assignedSubjects,
    ...studentNotes,
    ...staticDatas
};
