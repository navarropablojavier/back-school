module.exports = {
    '/api/subjects':{
        get: {
            security: [],
            summary: 'Subject to do list',
            parameters: [],
            responses: {
                200: {
                    description: 'Subjects to do list',
                    content: {
                        'application/json': {
                            schema: {
                                type: 'array',
                                items: {$ref: '#/components/schemas/Subjects'}
                            }
                        }
                    }
                },
                default: {
                    description: 'Error',
                    content: {'application/json': {schema: {$ref: '#/components/schemas/Error'}}}
                }
            }
        },
        post: {
            security: [],
            requestBody: {
                description: 'Optional description in *Markdown*',
                required: true,
                content: {'application/json': {schema: {$ref: '#/components/schemas/Subjects'}}}
            },
            responses: {
                200: {
                    description: 'Subjects to do list',
                    content: {
                        'application/json': {
                            schema: {
                                type: 'object',
                                properties: {}
                            }
                        }
                    }
                },
                default: {
                    description: 'Error',
                    content: {'application/json': {schema: {$ref: '#/components/schemas/Error'}}}
                }
            }
        }
    },
    '/api/subjects/{id}': {
        get: {
            security: [],
            parameters: [{$ref: '#/components/parameters/ID'} ],
            responses: {
                200 : {
                    description: 'Subject to do list',
                    content: {'application/json': {schema: {$ref: '#/components/schemas/Subjects'}}}
                },
                default: {
                    description: 'Error',
                    content: {'application/json': {schema: {$ref: '#/components/schemas/Error'}}}
                }
            }
        },
        put: {
            security: [],
            parameters: [{$ref: '#/components/parameters/ID'} ],
            requestBody:{
                description: 'Optional description in *Markdown*',
                required: true,
                content: {'application/json': {schema: {$ref: '#/components/schemas/Subjects'}}}
            },
            responses: {
                200: {
                    description: 'Subjects to do list ',
                    content: {
                        'application/json': {
                            schema: {
                                type: 'object',
                                properties:{}
                            }
                        }
                    }
                },
                default: {
                    description: 'Error',
                    content: {'application/json': {schema: {$ref: '#/components/schemas/Error'}}}
                }
            }
        },
        delete: {
            security : [],
            parameters: [{$ref: '#/components/parameters/ID'} ],
            responses: {
                200: {
                    description: 'Subjects to do list',
                    content: {
                        'application/json': {
                            schema: {
                                type:'object',
                                properties: {}
                            }
                        }
                    }
                },
                default: {
                    description: 'Error',
                    content: {'application/json': {schema: {$ref: '#/components/schemas/Error'}}}
                }
            }
        }
    }
};
