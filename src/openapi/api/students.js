module.exports = {
    '/api/students': {
        get: {
            security: [],
            summary: 'List of students',
            parameters: [],
            responses: {
                200: {
                    description: 'List of students',
                    content: {
                        'application/json': {
                            schema: {
                                type: 'array',
                                items: {$ref: '#/components/schemas/Students'}
                            }
                        }
                    }
                },
                default: {
                    description: 'Error',
                    content: {'application/json': {schema: {$ref: '#/components/schemas/Error'}}}
                }
            }
        },
        post: {
            security: [],
            requestBody: {
                description: 'Optional description in *Markdown*',
                required: true,
                content: {'application/json': {schema: {$ref: '#/components/schemas/Students'}}}
            },
            responses: {
                200: {
                    description: 'List of students',
                    content: {
                        'application/json': {
                            schema: {
                                type: 'object',
                                properties: {}
                            }
                        }
                    }
                },
                default: {
                    description: 'Error',
                    content: {'application/json': {schema: {$ref: '#/components/schemas/Error'}}}
                }
            }
        }
    },
    '/api/students/{id}' : {
        get: {
            security: [],
            summary: 'list of students',
            parameters: [{$ref: '#/components/parameters/ID'} ],
            responses: {
                200: {
                    description: 'list of Students',
                    content: {'application/json': {schema: {$ref: '#/components/schemas/Students'}}}
                },
                default: {
                    description: 'Error',
                    content: {'application/json': {schema: {$ref: '#/components/schemas/Error'}}}
                }
            }
        },
        put: {
            security: [],
            parameters: [{$ref: '#/components/parameters/ID'} ],
            requestBody: {
                description: 'Optional description in *Markdown*',
                required: true,
                content: {'application/json': {schema: {$ref: '#/components/schemas/Students'}}}
            },
            responses:{
                200: {
                    description: 'List of students',
                    content: {
                        'application/json': {
                            schema: {
                                type: 'object',
                                properties: {}
                            }
                        }
                    }
                },
                default: {
                    description: 'Error',
                    content: {'application/json': {schema: {$ref: '#/components/schemas/Error'}}}
                }
            }
        },
        delete: {
            security: [],
            parameters: [{$ref: '#/components/parameters/ID'} ],
            responses: {
                200: {
                    description: 'List of students',
                    content: {
                        'application/json': {
                            schema: {
                                type: 'object',
                                properties: {}
                            }
                        }
                    }
                },
                default: {
                    description: 'Error',
                    content: {'application/json': {schema: {$ref:'#/components/schemas/Error'}}}
                }
            }

        }
    }
};
