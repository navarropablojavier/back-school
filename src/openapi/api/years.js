module.exports = {
    '/api/years': {
        get: {
            security: [],
            summary: 'List Years',
            parameters: [],
            responses: {
                200: {
                    description: 'list of Years',
                    content: {
                        'application/json': {
                            schema: {
                                type: 'array',
                                items: { $ref: '#/components/schemas/Years' }
                            }
                        }
                    }
                },
                default: {
                    description: 'Error',
                    content: {'application/json': {schema: { $ref: '#/components/schemas/Years' }}}
                }
            }
        },
        post: {
            security: [],
            requestBody: {
                description: 'Optional description in *Markdown*',
                required: true,
                content: {'application/json': {schema: { $ref: '#/components/schemas/Years' }}}
            },
            responses: {
                200: {
                    description: 'list of Years',
                    content: {
                        'application/json': {
                            schema: {
                                type: 'object',
                                properties: {}
                            }
                        }
                    }
                },
                default: {
                    description: 'Error',
                    content: {'application/json': {schema: { $ref: '#/components/schemas/Error' }}}
                }
            }
        }
    },
    '/api/Years/{id}': {
        get: {
            security: [],
            parameters: [{$ref: '#/components/parameters/ID'}],
            responses: {
                200: {
                    description: 'list of Years',
                    content: {'application/json': {schema: { $ref: '#/components/schemas/Years' }}}
                },
                default: {
                    description: 'Error',
                    content: {'application/json': {schema: { $ref: '#/components/schemas/Error' }}}
                }
            }
        },
        put: {
            security: [],
            parameters: [{$ref: '#/components/parameters/ID'} ],
            requestBody: {
                description: 'Optional description in *Markdown*',
                required: true,
                content: {'application/json': {schema: { $ref: '#/components/schemas/Years' }}}
            },
            responses: {
                200: {
                    description: 'list of Years',
                    content: {
                        'application/json': {
                            schema: {
                                type: 'object',
                                properties: {}
                            }
                        }
                    }
                },
                default: {
                    description: 'Error',
                    content: {'application/json': {schema: { $ref: '#/components/schemas/Error' }}}
                }
            }
        },
        delete: {
            security: [],
            parameters: [{$ref: '#/components/parameters/ID'}],
            responses: {
                200: {
                    description: 'list of Years',
                    content: {
                        'application/json': {
                            schema: {
                                type: 'object',
                                properties: {}
                            }
                        }
                    }
                },
                default: {
                    description: 'Error',
                    content: {'application/json': {schema: { $ref: '#/components/schemas/Error' }}}
                }
            }
        }
    }
};
