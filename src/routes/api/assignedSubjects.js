const {AssignedSubjectsController }= include('controllers');

module.exports = router => {
    router.route('/')
        .get(AssignedSubjectsController.fetch)
        .post(AssignedSubjectsController.create);
    router.route('/:id')
        .get(AssignedSubjectsController.fetchOne)
        .put(AssignedSubjectsController.save)
        .delete(AssignedSubjectsController.delete);
    return router;
};
