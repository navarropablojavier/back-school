const {StudentNotesController } = include ('controllers');

module.exports = router => {
    router.route('/')
        .get(StudentNotesController.fetch)
        .post(StudentNotesController.create);
    router.route('/:id')
        .get(StudentNotesController.fetchOne)
        .put(StudentNotesController.save)
        .delete(StudentNotesController.delete);

    return router;
};
