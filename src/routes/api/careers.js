const {CareersController} = include('controllers');

module.exports = router => {
    router.route('/')
        .get(CareersController.fetch)
        .post(CareersController.create);
    router.route('/:id')
        .get(CareersController.fetchOne)
        .put(CareersController.save)
        .delete(CareersController.delete);
    return router;
};
