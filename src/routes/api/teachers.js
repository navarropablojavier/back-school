const { TeachersController } = include('controllers');

module.exports = router => {
    router.route('/')
        .get(TeachersController.fetch)
        .post(TeachersController.create);
    router.route('/:id')
        .get(TeachersController.fetchOne)
        .put(TeachersController.save)
        .delete(TeachersController.delete);
    return router;
};
