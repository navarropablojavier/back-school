const {SubjectsController} = include('controllers');

module.exports = router => {
    router.route('/')
        .get(SubjectsController.fetch)
        .post(SubjectsController.create);
    router.route('/:id')
        .get(SubjectsController.fetchOne)
        .put(SubjectsController.save)
        .delete(SubjectsController.delete);
    return router;

};
