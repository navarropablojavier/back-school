const {StudentsController} = include('controllers');

module.exports = router => {
    router.route('/')
        .get(StudentsController.fetch)
        .post(StudentsController.create);
    router.route('/:id')
        .get(StudentsController.fetchOne)
        .put(StudentsController.save)
        .delete(StudentsController.delete);
    return router;
};
