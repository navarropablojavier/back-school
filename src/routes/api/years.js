const { YearsController } = include('controllers');

module.exports = router => {
    router.route('/').get(YearsController.fetch).post(YearsController.create);

    router
        .route('/:id')
        .get(YearsController.fetchOne)
        .put(YearsController.save)
        .delete(YearsController.delete);

    return router;
};
