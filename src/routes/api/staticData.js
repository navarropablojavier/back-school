const {StaticDatasController} = include('controllers');

module.exports = router => {
    router.route('/')
        .get(StaticDatasController.fetch);
    return router;
};
