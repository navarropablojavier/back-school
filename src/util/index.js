const join = require('lodash/join');
const pick = require('lodash/pick');
const reduce = require('lodash/reduce');
const get = require('lodash/get');
const values = require('lodash/values');
const sum = require('lodash/sum');
const size = require('lodash/size');

const reducedList = (array, filterKey, keyData) => reduce(array, (objectsByKeyValue, obj) => {
    const value = join(values(pick(obj, filterKey)), '');
    objectsByKeyValue[value] = (objectsByKeyValue[value] || []).concat({
        name: get(obj, keyData),
        _id: get(obj, keyData)
    });
    return objectsByKeyValue;
}, {});

const averagesNote = notes => sum(notes)/size(notes);

module.exports = {
    reducedList,
    averagesNote
};
